import { Sequelize, Op } from 'sequelize';
import fs from 'fs';
import path from 'path';

let datasource = null;

module.exports = (app) => {
  if (datasource !== null) return datasource;

  const db = app.config.database;

  const sequelize = new Sequelize(db.name, db.user, db.password, {
    authenticate: {
      userName: db.user,
      password: db.password,
      domain: db.host,
    },
    host: db.host,
    dialect: db.dialect,
    protocol: db.protocol,
    port: db.port,
    password: db.password,
    define: db.define,
    sync: db.sync,
    pool: db.pool,
    encrypt: db.encrypt,
    operatorsAliases: Op,
    logging: app.config.debug.available
      ? (msg, queryExecutionTime) => {
        const logData = {
          query: msg,
        };
        if (app.config.debug.available) {
          logData.queryExecutionTime = `${queryExecutionTime} milliseconds`;
        }
        if (queryExecutionTime > app.config.debug.queryMaxTimeToNotice) {
          logData.msg = 'SLOW QUERY';
          app.logger.warn(logData);
        } else {
          logData.msg = 'GOOD QUERY';
          app.logger.info(logData);
        }
      }
      : false,
    benchmark: app.config.debug.available,
  });

  sequelize
    .authenticate()
    .then(() => app.logger.info('Connection has been established successfully.'))
    .catch(err => app.logger.error('Unable to connect to the database:', err));

  datasource = {
    sequelize,
    Sequelize,
    models: {},
  };

  const dir = path.join(__dirname, '../models/model');

  fs.readdirSync(dir).forEach((file) => {
    const modelDir = path.join(dir, file);
    const model = sequelize.import(modelDir);

    if (model) {
      datasource.models[model.name] = model;
    }
  });

  Object.keys(datasource.models).forEach((key) => {
    if ('associate' in datasource.models[key]) {
      datasource.models[key].associate(datasource.models);
    }
  });

  return datasource;
};
