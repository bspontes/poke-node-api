module.exports = (app) => {
  const Common = app.models.common.return;

  class PokemonMapper {
    static async modelToViewModel(model) {
      const base = Common.base();

      try {
        if (!model) return base;

        const pokemon = Object.assign({}, app.models.view.pokemon);

        pokemon.Id = model.Id;
        pokemon.Name = model.Name;
        pokemon.Status = model.Status;
        pokemon.HitPoint = model.HitPoint;
        pokemon.Type1 = model.Type1;
        pokemon.Type2 = model.Type2;
        pokemon.Description = model.Description;
        pokemon.PhysicalAttack = model.PhysicalAttack;
        pokemon.PhysicalDefense = model.PhysicalDefense;
        pokemon.SpecialAttack = model.SpecialAttack;
        pokemon.SpecialDefense = model.SpecialDefense;
        pokemon.Speedy = model.Speedy;
        pokemon.CreateAt = model.CreateAt;
        pokemon.UpdateAt = model.UpdateAt;
        pokemon.Accuracy = model.Accuracy;
        pokemon.GenderTypes = model.GenderTypes;
        pokemon.Order = model.Order;
        pokemon.Generation = model.Generation;

        base.Data = pokemon;

        return base;
      } catch (error) {
        base.Success = false;
        base.Description = error;

        return base;
      }
    }

    static async modelsToViewsModel(models) {
      const base = Common.base();

      try {
        if (!models) return base;

        const views = [];

        models.forEach((model) => {
          const view = Object.assign({}, app.models.view.pokemon);

          view.Id = model.Id;
          view.Name = model.Name;
          view.Status = model.Status;
          view.HitPoint = model.HitPoint;
          view.Type1 = model.Type1;
          view.Type2 = model.Type2;
          view.Description = model.Description;
          view.PhysicalAttack = model.PhysicalAttack;
          view.PhysicalDefense = model.PhysicalDefense;
          view.SpecialAttack = model.SpecialAttack;
          view.SpecialDefense = model.SpecialDefense;
          view.Speedy = model.Speedy;
          view.CreateAt = model.CreateAt;
          view.UpdateAt = model.UpdateAt;
          view.Accuracy = model.Accuracy;
          view.GenderTypes = model.GenderTypes;
          view.Order = model.Order;
          view.Generation = model.Generation;

          views.push(view);
        });

        base.Data = views;

        return base;
      } catch (error) {
        base.Success = false;
        base.Description = error;

        return base;
      }
    }
  }

  return PokemonMapper;
};
