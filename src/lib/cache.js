import NodeCache from 'node-cache';

module.exports = (app) => {
  const Common = app.models.common.return;
  const Config = app.config;
  const Client = new NodeCache({ stdTTL: Config.cache.timeout, checkperiod: Config.cache.timeout * 0.2, useClones: false });

  class CacheClient {
    static async get(key) {
      let base = Common.base();

      try {
        if (!Config.cache.available) return base;

        const data = await Client.get(key);

        if (!data) return base;

        base = JSON.parse(data);

        return base;
      } catch (error) {
        base.Success = false;
        base.Description = error;

        return base;
      }
    }

    static async set(key, data, params) {
      const base = Common.base();

      try {
        if (!Config.cache.available || !params.available) return base;

        if (!data.Data) return data;

        await Client.set(key, JSON.stringify(data), params.timeout);

        return base;
      } catch (error) {
        base.Success = false;
        base.Description = error;

        return base;
      }
    }
  }

  return CacheClient;
};
