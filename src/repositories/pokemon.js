module.exports = (app) => {
  const PokemonModel = app.database.database.models.Pokemon;
  const Common = app.models.common.return;

  class PokemonRepository {
    static async list() {
      const base = Common.base();

      try {
        const model = await PokemonModel.findAll({
          attributes: ['Id', 'Name', 'Status', 'HitPoint', 'Type1', 'Type2', 'Description',
            'PhysicalAttack', 'PhysicalDefense', 'SpecialAttack', 'SpecialDefense', 'Speedy',
            'CreateAt', 'UpdateAt', 'Accuracy', 'GenderTypes', 'Order', 'Generation'],
        });

        if (model) {
          base.Data = model.map(mdo => mdo.dataValues);
        }
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async getById(id) {
      const base = Common.base();

      try {
        const model = await PokemonModel.findOne({
          attributes: ['Id', 'Name', 'Status', 'HitPoint', 'Type1', 'Type2', 'Description',
            'PhysicalAttack', 'PhysicalDefense', 'SpecialAttack', 'SpecialDefense', 'Speedy',
            'CreateAt', 'UpdateAt', 'Accuracy', 'GenderTypes', 'Order', 'Generation'],
          where: {
            Id: id,
          },
        });

        if (model) {
          base.Data = model.dataValues;
        }
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async insert(model) {
      const base = Common.base();

      try {
        await PokemonModel.create(model);
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async update(model) {
      const base = Common.base();

      try {
        await PokemonModel.update(model, {
          where: {
            Id: model.id,
          },
        });
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async delete(id) {
      const base = Common.base();

      try {
        await PokemonModel.destroy({
          where: {
            Id: id,
          },
        });
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }
  }

  return PokemonRepository;
};
