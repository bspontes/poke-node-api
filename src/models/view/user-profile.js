module.exports = {
  Id: null,
  Name: null,
  TaxId: null,
  Email: null,
  Password: null,
  Active: false,
  CreateAt: null,
  UpdateAt: null,
  RememberMe: false,
};
