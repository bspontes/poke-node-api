module.exports = {
  Id: 0,
  Name: null,
  Status: 0,
  HitPoint: 0,
  Type1: 0,
  Type2: 0,
  Description: null,
  PhysicalAttack: 0,
  PhysicalDefense: 0,
  SpecialAttack: 0,
  SpecialDefense: 0,
  Speedy: 0,
  CreateAt: null,
  UpdateAt: null,
  Accuracy: 0,
  GenderTypes: 0,
  Order: 0,
  Generation: 0,
};
