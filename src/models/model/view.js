/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('View', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Icon: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: '(NULL)',
    },
    Text: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    ControllerId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'Controller',
        key: 'Id',
      },
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    MaxLevelAccess: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    SubViewId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'SubView',
        key: 'Id',
      },
    },
  }, {
    tableName: 'View',
  });
};
