/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Skill', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PowerPoint: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Power: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    Accuracy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    TypePower: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    Category: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'Skill',
  });
};
