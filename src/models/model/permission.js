/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Permission', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    AccessGroupId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'AccessGroup',
        key: 'Id',
      },
    },
    ViewId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'View',
        key: 'Id',
      },
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    LevelAccess: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    Order: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '((0))',
    },
  }, {
    tableName: 'Permission',
  });
};
