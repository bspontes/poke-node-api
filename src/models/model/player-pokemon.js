/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('PlayerPokemon', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Level: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    HitPoint: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PhysicalAttack: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PhysicalDefense: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    SpecialAttack: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    SpecialDefense: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Speedy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PowerPoint: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PokemonId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Pokemon',
        key: 'Id',
      },
    },
    PlayerId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Player',
        key: 'Id',
      },
    },
    ActiveSkillId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ActiveSkill',
        key: 'Id',
      },
    },
    Type: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    TypeDebuff: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Accuracy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Gender: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'PlayerPokemon',
  });
};
