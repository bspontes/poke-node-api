/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('UserRules', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    UserProfileId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'UserProfile',
        key: 'Id',
      },
    },
    AccessGroupId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'AccessGroup',
        key: 'Id',
      },
    },
  }, {
    tableName: 'UserRules',
  });
};
