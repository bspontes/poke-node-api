/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Player', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Level: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    MaxWeight: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    AccountId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Account',
        key: 'Id',
      },
    },
    StatusPlayer: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    PositionX: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: '((0))',
    },
    PositionY: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: '((0))',
    },
    PositionZ: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: '((0))',
    },
    Scene: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '((0))',
    },
  }, {
    tableName: 'Player',
  });
};
