module.exports = function (sequelize, DataTypes) {
  return sequelize.define('UserProfile', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    TaxId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    RememberMe: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: '0',
    },
  }, {
    tableName: 'UserProfile',
  });
};
