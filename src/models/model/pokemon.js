/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Pokemon', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    HitPoint: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Type1: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Type2: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    Description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    PhysicalAttack: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PhysicalDefense: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    SpecialAttack: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    SpecialDefense: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Speedy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'CreateAt',
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    Accuracy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    GenderTypes: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Generation: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'Pokemon',
    freezeTableName: true,
  });
};
