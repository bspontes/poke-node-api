/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('PokemonSkill', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    PokemonId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Pokemon',
        key: 'Id',
      },
    },
    SkillId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Skill',
        key: 'Id',
      },
    },
    StatusSkill: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    LevelRequired: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'PokemonSkill',
  });
};
