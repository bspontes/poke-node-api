/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Item', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Status: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Price: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    Weight: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
    Description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    TypeItem: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  }, {
    tableName: 'Item',
  });
};
