/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('ActiveSkill', {
    Id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    Order1: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Order2: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Order3: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Order4: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Skill1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Skill',
        key: 'Id',
      },
    },
    Skill2: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Skill',
        key: 'Id',
      },
    },
    Skill3: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Skill',
        key: 'Id',
      },
    },
    Skill4: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'Skill',
        key: 'Id',
      },
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
  }, {
    tableName: 'ActiveSkill',
  });
};
