/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Controller', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    Name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    CreateAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    UpdateAt: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
  }, {
    tableName: 'Controller',
  });
};
