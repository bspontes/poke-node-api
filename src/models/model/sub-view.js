/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('SubView', {
    Id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    Description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Icon: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    Update_at: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'UpdateAt',
    },
    Create_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  }, {
    tableName: 'SubView',
  });
};
