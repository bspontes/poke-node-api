module.exports = (app) => {
  const Param = app.constants.params.pokemon;
  const Repository = app.repositories.pokemon;
  const Mapper = app.mappers.pokemon;
  const Common = app.models.common.return;
  const Redis = app.lib.redis;
  const Cache = app.lib.cache;

  class PokemonService {
    static async list() {
      const pokemon = `${Param.name}`;
      const base = Common.base();

      try {
        const cacheGet = await Cache.get(pokemon);

        if (!cacheGet.Success) return cacheGet;
        if (cacheGet.Data) return cacheGet;

        const redisGet = await Redis.get(pokemon);

        if (!redisGet.Success) return redisGet;
        if (redisGet.Data) return redisGet;

        const repository = await Repository.list();

        if (!repository.Success) return repository;

        const mapper = await Mapper.modelsToViewsModel(repository.Data);

        if (!mapper.Success) return mapper;

        const cacheSet = await Cache.set(pokemon, mapper, Param.cache);

        if (!cacheSet.Success) return cacheSet;

        const redisSet = await Redis.set(pokemon, mapper, Param.redis);

        if (!redisSet.Success) return redisSet;

        base.Data = mapper.Data;
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async getById(id) {
      const pokemon = `${Param.name}-${id}`;
      const base = Common.base();

      try {
        const cacheGet = await Cache.get(pokemon);

        if (!cacheGet.Success) return cacheGet;
        if (cacheGet.Data) return cacheGet;

        const redisGet = await Redis.get(pokemon);

        if (!redisGet.Success) return redisGet;
        if (redisGet.Data) return redisGet;

        const repository = await Repository.getById(id);

        if (!repository.Success) return repository;

        const mapper = await Mapper.modelToViewModel(repository.Data);

        if (!mapper.Success) return mapper;

        const cacheSet = await Cache.set(pokemon, mapper, Param.cache);

        if (!cacheSet.Success) return cacheSet;

        const redisSet = await Redis.set(pokemon, mapper, Param.redis);

        if (!redisSet.Success) return redisSet;

        base.Data = mapper.Data;
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }

    static async insert(model) {
      const base = Common.base();

      try {
        const repository = await Repository.insert(model);

        if (!repository.Success) return repository;

        const mapper = await Mapper.modelToViewModel(repository.Data);

        if (!mapper.Success) return mapper;

        base.Data = mapper.Data;
      } catch (error) {
        base.Success = false;
        base.Description = error;
      }

      return base;
    }
  }

  return PokemonService;
};
