module.exports = {
  debug: {
    level: 'debug',
    available: true,
    local: true,
  },
  database: {
    host: 'pokeportal.cz25lgynpq6d.sa-east-1.rds.amazonaws.com',
    port: 1433,
    name: 'Poke',
    dialect: 'mssql',
    user: 'bspontes',
    password: 'Bru123bru123',
    encrypt: true,
    protocol: 'tcp',
    define: {
      underscored: false,
    },
    sync: {
      force: true,
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
  cache: {
    available: true,
    timeout: 5000,
  },
  redis: {
    available: true,
    host: 'localhost',
    port: '6379',
    timeout: 5000,
  },
};
