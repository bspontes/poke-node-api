import express from 'express';
import consign from 'consign';
import { join } from 'path';

const app = express();

consign({
  cwd: join(__dirname, ''),
  extensions: ['.js'],
  verbose: false,
})
  .include('./config/config.js')
  .then('./database/database.js')
  .then('./constants/params/')
  .then('./models/common/')
  .then('./lib/')
  .then('./middlewares/')
  .then('./repositories/')
  .then('./mappers/')
  .then('./services/')
  .then('./models/view/')
  .then('./controllers/')
  .then('./routes/')
  .into(app);

app.listen(app.listen(3000, () => app.logger.info('Service has been started successfully')));

export default app;
