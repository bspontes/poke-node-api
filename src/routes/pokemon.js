module.exports = (app) => {
  const controller = app.controllers.pokemon;

  app.get('/pokemon', controller.getById);

  app.get('/pokemon/list', controller.list);
};
