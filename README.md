# poke-node-api

## Configuração de ambiente

### Instalação

Para Instalar todas as dependências do projeto

```bash
npm install
```

### Build

Para gerar a versão que será executada pela aplicação

```bash
npm run build
```

### lint

Para verificar se o código está dentro do padrão standard js

```bash
npm run lint
```

Para verificar e corrigir automaticamente para o padrão standard js, caso não corrija ele aponta onde deve ajustar

```bash
npm run lint:fix
```

### Execução

#### sincrono

Usoado para startar a aplicação sem observar as mudanças em tempo real.

```bash
npm start
```

#### assincrono

Usado durante o desenvolvimento para enxergar em tempo real as mudanças sem restartar.

```bash
npm run startdev
```

#### modo debug

Caso queira depurar enquanto desenvolve

```bash
npm run debug
```

Caso você use [VSCode](https://code.visualstudio.com/docs/editor/debugging) você pode usar a configuração abaixo e depurar em tempo de execução.

[.vscode/launch.json]

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Launch via NPM",
      "runtimeExecutable": "npm",
      "runtimeArgs": ["run", "debug"],
      "port": 5000
    }
  ]
}
```
